﻿import java.util.Scanner;

class Fahrkartenautomat {

	public static void main(String[] args) {

		double zuZahlenderBetrag;
		double rückgabebetrag;

		for (;;) { // Endlosschleife für den Programmstart

			// Bestellung erfassen
			zuZahlenderBetrag = fahrkartenbestellungErfassen();

			// Fahrkarte bezahlen
			rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

			// Fahrscheinausgabe
			fahrkartenAusgeben();

			// Rückgeldausgabe
			rueckgeldAusgeben(rückgabebetrag);

			System.out.println("\nVergessen Sie nicht, den Fahrschein vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wünschen Ihnen eine gute Fahrt.\n\n");

		}
	}

	// Methoden
	// -------------------------------

	public static double fahrkartenbestellungErfassen() {
		// Rückgabewert: zuZahlenderBetrag

		double einzelpreis;
		byte anzahlTickets;
		double zuZahlenderBetrag;

		Scanner tastatur = new Scanner(System.in);

		// Erstellung und Initialisierung des Arrays Fahrkartenbezeichnung
		String[] Fahrkartenbezeichnung = new String[11];

		Fahrkartenbezeichnung[1] = "Einzelfahrschein Berlin AB ";
		Fahrkartenbezeichnung[2] = "Einzelfahrschein Berlin BC";
		Fahrkartenbezeichnung[3] = "Einzelfahrschein Berlin ABC";
		Fahrkartenbezeichnung[4] = "Kurzstrecke";
		Fahrkartenbezeichnung[5] = "Tageskarte Berlin AB";
		Fahrkartenbezeichnung[6] = "Tageskarte Berlin BC";
		Fahrkartenbezeichnung[7] = "Tageskarte Berlin ABC";
		Fahrkartenbezeichnung[8] = "Kleingruppen-Tageskarte Berlin AB";
		Fahrkartenbezeichnung[9] = "Kleingruppen-Tageskarte Berlin B";
		Fahrkartenbezeichnung[10] = "Kleingruppen-Tageskarte Berlin ABC";

		// Erstellung und Initialisierung des Arrays Farhkartenpreise
		Double[] Fahrkartenpreise = new Double[11];
		Fahrkartenpreise[1] = 2.90;
		Fahrkartenpreise[2] = 3.30;
		Fahrkartenpreise[3] = 3.60;
		Fahrkartenpreise[4] = 1.90;
		Fahrkartenpreise[5] = 8.60;
		Fahrkartenpreise[6] = 9.00;
		Fahrkartenpreise[7] = 9.60;
		Fahrkartenpreise[8] = 23.50;
		Fahrkartenpreise[9] = 24.30;
		Fahrkartenpreise[10] = 24.90;

		// Bestellauswahl

		System.out.println("Fahrkartenbestellvorgang:\n" + "=========================\n");
		System.out.printf("%-15s %-40s %-15s", "Auswahlnummer", "Bezeichnung", "Preis in Euro \n");
		System.out.printf("%-15s %-40s %-15.2f \n", "1", Fahrkartenbezeichnung[1], Fahrkartenpreise[1]);
		System.out.printf("%-15s %-40s %-15.2f \n", "2", Fahrkartenbezeichnung[2], Fahrkartenpreise[2]);
		System.out.printf("%-15s %-40s %-15.2f \n", "3", Fahrkartenbezeichnung[3], Fahrkartenpreise[3]);
		System.out.printf("%-15s %-40s %-15.2f \n", "4", Fahrkartenbezeichnung[4], Fahrkartenpreise[4]);
		System.out.printf("%-15s %-40s %-15.2f \n", "5", Fahrkartenbezeichnung[5], Fahrkartenpreise[5]);
		System.out.printf("%-15s %-40s %-15.2f \n", "6", Fahrkartenbezeichnung[6], Fahrkartenpreise[6]);
		System.out.printf("%-15s %-40s %-15.2f \n", "7", Fahrkartenbezeichnung[7], Fahrkartenpreise[7]);
		System.out.printf("%-15s %-40s %-15.2f \n", "8", Fahrkartenbezeichnung[8], Fahrkartenpreise[8]);
		System.out.printf("%-15s %-40s %-15.2f \n", "9", Fahrkartenbezeichnung[9], Fahrkartenpreise[9]);
		System.out.printf("%-15s %-40s %-15.2f \n\n", "10", Fahrkartenbezeichnung[10], Fahrkartenpreise[10]);

		// Eingabeauforderung Auswahl

		System.out.print("Ihre Wahl: ");
		int auswahlTicket = tastatur.nextInt();

		// Ausschluss Falscheingabe

		while (auswahlTicket >= 11 || auswahlTicket <= 0) {
			System.out.println(" >> falsche Eingabe <<");
			System.out.print("Ihre Wahl: ");
			auswahlTicket = tastatur.nextInt();
		}

		// Preiszuweisung

		einzelpreis = Fahrkartenpreise[auswahlTicket];

		// Eingabe Anzahl Tickets

		System.out.print("Anzahl der Tickets: ");
		anzahlTickets = tastatur.nextByte();

		// Berechnung zu zahlender Betrag

		zuZahlenderBetrag = einzelpreis * anzahlTickets;

		return zuZahlenderBetrag;

	}

	public static double fahrkartenBezahlen(double zuZahlen) {
		// Rückgabewert: Rückgeld dezimal

		double eingezahlterGesamtbetrag = 0.00;
		double eingeworfeneMünze;

		Scanner tastatur = new Scanner(System.in);

		while (eingezahlterGesamtbetrag < zuZahlen) {
			System.out.printf("Noch zu zahlen: %.2f EURO \n", (zuZahlen - eingezahlterGesamtbetrag));

			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;

		}

		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlen;

		return rückgabebetrag;

	}

	public static void fahrkartenAusgeben() {

		System.out.println("\nFahrschein wird ausgegeben");

		warte(250);

	}

	public static void rueckgeldAusgeben(double rückgabebetrag) {
		// Rückgeld: Münzausgabe

		if (rückgabebetrag > 0.00) {
			System.out.printf("\n\nDer Rückgabebetrag in Höhe von %.2f EURO wird in folgenden Münzen ausgezahlt:\n\n",
					rückgabebetrag);

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				muenzeAusgeben(2, "EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				muenzeAusgeben(1, "EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{

				muenzeAusgeben(50, "CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				muenzeAusgeben(20, "CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				muenzeAusgeben(10, "CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.04)// 5 CENT-Münzen
			{
				muenzeAusgeben(5, "CENT");
				rückgabebetrag -= 0.05;
			}
		}

	}

	public static void warte(int millisekunde) {

		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(millisekunde);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}

	}

	public static void muenzeAusgeben(int betrag, String einheit) {

		System.out.println(betrag + " " + einheit);

	}

}
