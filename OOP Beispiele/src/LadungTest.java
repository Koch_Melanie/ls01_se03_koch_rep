 
public class LadungTest {

	public static void main(String[] args) {
		
		// Erstellen der Objekte
		
		Ladung l1 = new Ladung ("Ferengi Schneckensaft", 200);
		Ladung l2 = new Ladung ("Borg Schrott", 5);
		Ladung l3 = new Ladung ("Rote Materie", 2);
		Ladung l4 = new Ladung ("Forschungssonde", 35);
		
		
		// Konsolenausgabe 
		
		System.out.printf("Bezeichnung: %s \nMenge: %d \n\n", l1.getBezeichnung(), l1.getMenge());
		System.out.printf("Bezeichnung: %s \nMenge: %d \n\n", l2.getBezeichnung(), l2.getMenge());
		System.out.printf("Bezeichnung: %s \nMenge: %d \n\n", l3.getBezeichnung(), l3.getMenge());

	
	}

}
