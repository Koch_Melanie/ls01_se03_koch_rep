
public class Buch {

	// Attribute
	// ---------------------------

	private String titel;
	private double preis;

	// Konstruktur
	// ---------------------------

	public Buch() {

		this.titel = "Unbekannt";
		this.preis = 0;
	}

	// Getter & Setter
	// ---------------------------

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

}
