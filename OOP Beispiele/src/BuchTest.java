
public class BuchTest {

	public static void main(String[] args) {

		Buch b1 = new Buch();

		System.out.println("Titel: " + b1.getTitel() + "\nPreis: " + b1.getPreis());

		b1.setTitel("OOP");
		b1.setPreis(29.50);

		System.out.printf("Buchtitel: %s  \nPreis: %.2f � %n", b1.getTitel(), b1.getPreis());

	}

}
