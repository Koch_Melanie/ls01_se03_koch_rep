
public class RaumschiffTest {
	
	public static void main (String [] args) {
	
		// Erstellen der Objekte
		
		Raumschiff klingonen = new Raumschiff (1, 100, 100, 100, 100, "IKS Hegh'ta", 2);
		
		Raumschiff rumulaner = new Raumschiff (2, 100, 100, 100, 100, "IRW Khazara", 2);
		
		Raumschiff vulkanier = new Raumschiff (0, 80, 80, 50, 100, "Ni'Var", 5);
		
		
		// Konsolenausgabe 
		
		//....Klingonen 
		
		System.out.printf("Anzahl Photonentorpedo: %d \n"
				+ "Energieversorgung (in Prozent): %d \n"
				+ "Schilde (in Prozent): %d \n"
				+ "H�lle (in Prozent): %d \n"
				+ "Lebenserhaltungssysteme (in Prozent): %d \n"
				+ "Schiffname: %s \n"
				+ "Anzahl der Androiden: %d \n\n ", klingonen.getPhotonentorpedoAnzahl(),
				klingonen.getEnergieversorgungInProzent(), klingonen.getSchildeInProzent(), klingonen.getHuelleInProzent(),
				klingonen.getLebenserhaltungssystemeInProzent(), klingonen.getSchiffsname(), klingonen.getAndroidenAnzahl());
		
		//....Rumulaner
		
		System.out.printf("Anzahl Photonentorpedo: %d \n"
				+ "Energieversorgung (in Prozent): %d \n"
				+ "Schilde (in Prozent): %d \n"
				+ "H�lle (in Prozent): %d \n"
				+ "Lebenserhaltungssysteme (in Prozent): %d \n"
				+ "Schiffname: %s \n"
				+ "Anzahl der Androiden: %d \n\n", rumulaner.getPhotonentorpedoAnzahl(),
				rumulaner.getEnergieversorgungInProzent(), rumulaner.getSchildeInProzent(), rumulaner.getHuelleInProzent(),
				rumulaner.getLebenserhaltungssystemeInProzent(), rumulaner.getSchiffsname(), rumulaner.getAndroidenAnzahl());
		
		
		//....Vulkanier
		
		System.out.printf("Anzahl Photonentorpedo: %d \n"
				+ "Energieversorgung (in Prozent): %d \n"
				+ "Schilde (in Prozent): %d \n"
				+ "H�lle (in Prozent): %d \n"
				+ "Lebenserhaltungssysteme (in Prozent): %d \n"
				+ "Schiffname: %s \n"
				+ "Anzahl der Androiden: %d \n\n", vulkanier.getPhotonentorpedoAnzahl(),
				vulkanier.getEnergieversorgungInProzent(), vulkanier.getSchildeInProzent(), vulkanier.getHuelleInProzent(),
				vulkanier.getLebenserhaltungssystemeInProzent(), vulkanier.getSchiffsname(), vulkanier.getAndroidenAnzahl());
		
		
		
//		klingonen.addLadung(test);
		
	}
}
