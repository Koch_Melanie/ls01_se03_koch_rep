
public class Ladung {

	// Attribute
	// -----------------------------------

	private String bezeichnung;
	private int menge;

	// Konstruktoren
	// -----------------------------------

	public Ladung() {

		this.bezeichnung = "Unbekannt";
		this.menge = 0;

	}

	public Ladung(String bezeichnung, int menge) {

		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}

	// Getter & Setter
	// -----------------------------------

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public int getMenge() {
		return menge;
	}

	public void setMenge(int menge) {
		this.menge = menge;
	}

}
