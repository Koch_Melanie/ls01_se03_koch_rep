import java.util.Scanner;

public class Iterationsbeispiele {

	public static void main(String[] args) {
		
		
		Scanner input = new Scanner (System.in);

		System.out.println("Geben Sie bitte n ein:");
		int n = input.nextInt();
		
		int zaehler = 0;
		int ergebnis = 0;
		
		while (zaehler <= n) {
			
			ergebnis = ergebnis + zaehler;
			zaehler++;
		}
	
		System.out.println("Summe = " + ergebnis);
	}

	
	

}
