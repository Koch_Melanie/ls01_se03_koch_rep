
/*   Aufgabe 2: Fakult�t
 *   
 *   Schreiben Sie ein Programm, das zu einer Zahl n <= 20 die Fakult�t n!
 *   ermittelt.
 *   
 *   Es gilt:
 *   n! = 1 * 2 * ... * (n-1) * n sowie 0! = 1* 
 * 	
 * 	 z.B. ist 3! = 1 * 2 * 3 = 6
 * 
 */

import java.util.Scanner;

public class fakultaet {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.print("Gebe eine Zahl ein, um die Fakult�t errechnen zu lassen:");
		int n = input.nextInt();

		if (n == 0) {

			System.out.print("0! = 1");
		} else {

			int zaehler = 1;
			int ergebnis = 1;

			while (n >= zaehler) {

				ergebnis = ergebnis * zaehler;
				zaehler++;

			}

			System.out.println(n + "! = " + ergebnis);

		}

	}
}
