import java.util.Scanner;

public class ForSchleifeSumme {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		System.out.print("Bitte n eingeben: ");
		int n = input.nextInt();

		System.out.println("");

		// Summe A
		// --------------------------------------

		int summeA = 0;
		for (int i = 1; i <= n; i++) {
			System.out.print(i + " ");
			summeA = summeA + i;
		}
		System.out.println("\nDie Summe f�r A betr�gt: " + summeA);

		System.out.println("");

		// Summe f�r B
		// --------------------------------------

		int summeB = 0;

		for (int i = 2; i <= n; i = i + 2) {
			System.out.print(i + " ");
			summeB = summeB + i;
		}
		System.out.println("\nDie Summe f�r B betr�gt: " + summeB);

		System.out.println("");

		// Summe f�r C
		// --------------------------------------

		int summeC = 0;

		for (int i = 1; i <= n; i = i + 2) {
			System.out.print(i + " ");
			summeC = summeC + i;
		}
		System.out.println("\nDie Summe f�r C betr�gt: " + summeC);
	}

}
