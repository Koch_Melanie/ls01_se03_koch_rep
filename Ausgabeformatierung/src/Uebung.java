
public class Uebung {

	public static void main (String [] args) {
		
		/*
		 * Ausgabe sollsein: 
		 
		
				Monitor : 103,45 Euro
				Tastatur: 45,95 Euro
		*/
		
		System.out.printf("Monitor: %.2f Euro %n Tastatur: %.2f Euro %n", 103.45, 45.95);
		
	}
	
}
