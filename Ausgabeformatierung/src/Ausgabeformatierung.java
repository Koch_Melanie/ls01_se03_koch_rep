
public class Ausgabeformatierung {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
/*		System.out.print("Hello");
		
		// System.out.println("Hello \n");
				
		System.out.println("Hello");
		
		
		System.out.println ("Preis: " + 2.90 + " Euro");
		
		// System.out.println ("Preis: \t" + 2.90 + " Euro");


	*/

		
		
		// String = Zeichenkette
		
		// ....................................................................
	
		/*
		System.out.printf(format, args)
		=
		System.out.printf("%s", "12345678");
		
		ich m�chte nur die ersten drei Zeichen angezeigt bekommen
		 
		System.out.printf("%.3s", "123456789")
		
		
		% = da kommt schlussendlich der Wert rein
		s = Zeichenkette
		20 = Die Zeichenkette soll in einem Wert von "20 Zeichenkette" geschrieben werden
		\n = Zeilenumbruch
		
		System.out.printf("|%-20s| \n", "123456789");
		
		mit Leerzeichen vor dem Wert:
		System.out.printf("|    %-20s| \n", "123456789");
		
		
		*/
		
		
		System.out.printf("|   %-20s| \n", "123456789");
		// |   123456789           | 
		
		
		// Ganzzahl
		// ....................................................................
		
		/*
		 * 
		 * d = dezimal = Ganzzahl
		 * 
		 */
		
		System.out.printf("| %20d | %n", 123456789);
		// |            123456789 | 
		
		System.out.printf("| %020d | %n", 123456789);
		// | 00000000000123456789 |
		
		
		// Kommazahlen
		// ....................................................................
		
		System.out.printf("| %f| \n", 12345.6789);
		// | 12345,678900| 
		System.out.printf("| %.2f| \n", 12345.6789);
		// | 12345,68| 
		System.out.printf("| %20.2f| \n", 12345.6789);
		// |             12345,68| 
		System.out.printf("| %-20.2f| \n", 12345.6789);
		// | 12345,68            | 

		
		
		  
		
	}

}
