



public class Mittelwert {

	   public static void main(String[] args) {

	      // (E) "Eingabe"
	      // Werte für x und y festlegen:
	      // ===========================
	      double x = 2.0;
	      double y = 4.0;
	      double m = 0;
	      

	      m = berechneMittelwert(x,y);
	      
	      
	      
	      ausgabeMittelwert(berechneMittelwert(x,y));

	      ausgabeMittelwert2 (x, y, m);
	      
	      
	   }
	   
	   
	   
	   public static double berechneMittelwert (double zahl1, double zahl2) {
		   
		   double ergebnis = (zahl1 + zahl2) / 2.0;
		   return ergebnis;
		   
	   }
	   
	   public static void ausgabeMittelwert (double ergebnis) {
		   
		   System.out.println(ergebnis);
		   
	   }
	


	   public static void ausgabeMittelwert2 (double zahl1, double zahl2, double m) {
	
	   
		   	System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", zahl1, zahl2, berechneMittelwert(zahl1, zahl2)); 
}

}








//------------------------------------------------------------------------------------------------------
//public class Mittelwert {
//
//   public static void main(String[] args) {
//
//      // (E) "Eingabe"
//      // Werte für x und y festlegen:
//      // ===========================
//      double x = 2.0;
//      double y = 4.0;
//      double m;
//      
//      // (V) Verarbeitung
//      // Mittelwert von x und y berechnen: 
//      // ================================
//      m = (x + y) / 2.0;
//      
//      // (A) Ausgabe
//      // Ergebnis auf der Konsole ausgeben:
//      // =================================
//      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
//   }
//}