import java.util.Scanner;

public class HardwareGrosshaendler {
	
	public static void main (String [] args) {
	
		Scanner inputInt = new Scanner (System.in);
		Scanner inputDouble = new Scanner (System.in);
		
		System.out.println("Wie viele PC-M�use m�chten Sie bestellen?");
		int bestellmenge = inputInt.nextInt();
		
		System.out.println("Bitte geben Sie den Einzelpreis ein: ");
		double einzelpreis = inputDouble.nextDouble();
	
		boolean lieferpauschale = berechneLieferpauschale (bestellmenge);
		double rechnungsbetrag = rechnungsBetrag (bestellmenge, einzelpreis, lieferpauschale);
		System.out.println("-------------------------");
		System.out.println("Rechnungsbetrag: " + rechnungsbetrag + " �");
	}
	
	
	
	public static boolean berechneLieferpauschale (int menge) {
		
		boolean abfrage = true;
		if (menge >= 10){
		
			System.out.println("Die Lieferung erfolgt frei Haus!");
			abfrage = false;
		}
		
		else
			if (menge <10) {
				
				System.out.println("Wir berechnen eine Lieferpauschale von 10,00 �!");
				abfrage = true;
		}
		
		return abfrage;
		}

	
	public static double rechnungsBetrag (int bestellmenge, double einzelpreis, boolean lieferpauschale) {
		
		double ergebnis = 0;
		
		if (lieferpauschale == true) {
			
			ergebnis = bestellmenge * einzelpreis + 10;
			
			
		}
		
		else {
			
			ergebnis = bestellmenge * einzelpreis;
		}
		
		return ergebnis;
	}
	
}
