import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Benutzerverwaltung {

	private static ArrayList<Benutzer> benutzerliste = new ArrayList<Benutzer>();

	public static void main(String[] args) {

		int auswahl;

		// Men�auswahl
		do {
			auswahl = menu();
			switch (auswahl) {
			case 1:
				// Benutzer anzeigen ---------------
				benutzerAnzeigen();
				System.out.println("");
				break;
			case 2:
				// Benutzer erfassen ---------------
				benutzerErfassen();
				break;
			case 3:
				// Benutzer l�schen ---------------
				benutzerLoeschen();
				break;
			case 4:
				// Programm beenden ---------------
				System.exit(0);
			default:
				System.err.println("\nFalsche Eingabe.\n");
			}
		} while (true);

	}

	// Methoden
	// ----------------------------------------------------------------------------

	public static void benutzerAnzeigen() {

		// If/Else-Verzweigung
		// Wenn keine Benutzer hinterlegt, dann springt der User wieder ins Hauptmen�
		if (benutzerliste.size() == 0) {

			System.out.println("\nAktuell sind keine Benutzer gespeichert!\n");
		}

		else {

			System.out.println("\nAktuelle Benutzer: \n-------------------\n");

			for (int i = 0; i < benutzerliste.size(); i++) {

				System.out.println(benutzerliste.get(i).toString());
			}

		}

	}

	public static void benutzerErfassen() {

		// Lokale Variablen
		String name;
		int benutzernummer;

		// Scanner
		Scanner input = new Scanner(System.in);

		// Eingabeaufforderung
		System.out.print("Bitte Namen eingeben: ");
		name = input.next();
		System.out.print("Bitte Benutzernummer eingeben: ");
		benutzernummer = input.nextInt();

		// Erstellen eines neuen Objektes/Element
		Benutzer b1 = new Benutzer(name, benutzernummer);

		// Aufnehmen in die ArrayList
		benutzerliste.add(b1);

		// Leerzeile
		System.out.println("");

	}

	public static void benutzerLoeschen() {

		int indexAuswahl;
		Scanner input = new Scanner(System.in);

		// If/Else-Verzweigung
		// Wenn keine Benutzer hinterlegt, dann springt der User wieder ins Hauptmen�
		if (benutzerliste.size() == 0) {

			System.out.println("\nAktuell sind keine Benutzer gespeichert!\n");
		}

		else {
			// Auflistung der aktuellen Benutzer mit Index
			System.out.printf("\n%-5S %-10S %-10S", "Index", "Name", "Benutzernummer");
			for (int i = 0; i < benutzerliste.size(); i++) {

				System.out.printf("\n%-5d %-10s %-10d", i, benutzerliste.get(i).getName(),
						benutzerliste.get(i).getBenutzernummer());

			}

			System.out.print("\n\nWelcher Benutzer soll gel�scht werden? \n" + "Bitte Index angeben: ");

			// Benutzereingabe
			indexAuswahl = input.nextInt();

			// Benutzer l�schen via Index
			benutzerliste.remove(indexAuswahl);

			System.out.println("\nDer Benutzer wurde erfolgreich gel�scht!\n");
		}

	}

	public static int menu() {

		int selection;
		Scanner input = new Scanner(System.in);

		/***************************************************/

		System.out.println("     Benutzerverwaltung     ");
		System.out.println("----------------------------\n");
		System.out.println("1 - Benutzer anzeigen");
		System.out.println("2 - Benutzer erfassen");
		System.out.println("3 - Benutzer l�schen");
		System.out.println("4 - Ende");

		System.out.print("\nEingabe:  ");
		selection = input.nextInt();
		return selection;
	}

}
