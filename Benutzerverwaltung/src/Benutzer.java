
public class Benutzer {

	// Attribute
	// ----------------------------------------

	private int benutzernummer;
	private String name;

	// Konstruktur
	// ----------------------------------------
	public Benutzer(String name, int benutzernummer) {

		this.benutzernummer = benutzernummer;
		this.name = name;
	}

	// Getter & Setter
	// ----------------------------------------

	public int getBenutzernummer() {
		return this.benutzernummer;
	}

	public void setBenutzernummer(int benutzernummer) {
		this.benutzernummer = benutzernummer;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	// Methoden
	// ----------------------------------------

	public String toString() {
		// Welche Attributwerte m�chte ich darstellen?
		// Name: xy, Benutzernummer: xxxxx1

		return "Name: " + this.name + " , Benutzernummer: " + this.benutzernummer;

	}

}
